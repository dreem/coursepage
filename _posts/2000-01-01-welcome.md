---
title: "home"
bg: '#f2d6f5'
color: '#4d4d4d'
style: center
---


![PARAFAC course](img/parafac_header.png)


"Parallel Factor Analysis for DOM fluorescence" is an online course run by Kathleen Murphy (Chalmers University of Technology) and Urban Wünsch (Technical University of Denmark).

In our course, you will learn how to analyze the fluorescence composition of dissolved organic matter using PARAFAC.
{::comment}
<span id="forkongithub">
  <a href="https://dtu.events/parafac-course-2024/signup" class="bg-blue">
    Sign up now & avoid late fee after 14/12
  </a>
</span>
{:/comment}

{::comment}
<span id="forkongithub">
  <a href="https://dtu.events/parafac-course-2024/signup" class="bg-blue">
    Sign up here
  </a>
</span>
{:/comment}