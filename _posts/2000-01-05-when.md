---
title: "When?"
bg: '#ffdeb3'
color: '#4d4d4d'
style: center
fa-icon: calendar
---


The course will take place from January 22nd to February 8th 2024. Sign-ups are now closed.

Please contact us to register your interest in participating in the course. There is no fixed schedule for our online course and we decide to run it based on having accumulated sufficient expressions of interest. From experience, this is the case about every 2nd year, so you can expect the next course to take place in spring of 2026. Express your interest [here](mailto:dreem@openfluor.net?subject=Expression%20of%20interest%20in%20PARAFAC%20course&body=Hi%20drEEM-team%2C%0D%0AI'd%20like%20to%20express%20my%20interest%20in%20your%20online%20course%20%22PARAFAC%20for%20DOM%20fluorescence%22.%20%0D%0A)

{::comment} Sign up by clicking on the banner in the top right corner of this screen. {:/comment}

