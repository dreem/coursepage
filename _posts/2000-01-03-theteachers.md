---
title: "The teachers"
bg: '#e6fffb'
color: '#4d4d4d'
style: center
fa-icon: users
---


# The team

### Kathleen (Kate) Murphy

![Kathleen Murphy](img/kate.jpg){: style="float: left"}

Kate's research focuses on the interpretation of fluorescence from dissolved organic matter in natural waters and the application of fluorescence for distinguishing between water sources and detecting changes in water quality. Recent research projects relate to detecting changes in organic matter character during recycled and drinking water treatment, and tracing the geographical origin of ships’ ballast water. She has instigated several open-source projects aimed at improving the modelling and interpretation of natural organic matter fluorescence, including the MATLAB toolbox drEEM and the online database of fluorescence spectra OpenFluor.

Kate is a [Associate Professor at Chalmers University of Technology](https://www.chalmers.se/en/staff/Pages/murphyk.aspx) (Gothenburg, Sweden)



### Urban Wünsch

![Urban Wünsch](img/urban.jpg){: style="float: right"}

Urban's research focusses on the analytical chemistry, ultraviolet-visible spectroscopy, chemometrics, and software development. Urban is an active contributor to the drEEM toolbox and is currently developing the the Environmental Data Fusion toolbox ([EDF toolbox](https://gitlab.com/urbanwuensch/edf)). He continues to design new approaches to deconvolute dissolved organic matter (DOM) fluorescence by combining analytical methods with the latest chemometric tools. His current projects  focus on photochemistry and enzymatic degradation of terrestrial organic matter.

Urban is a [postdoctoral researcher at the Technical University of Denmark](https://orbit.dtu.dk/en/persons/urban-w%C3%BCnsch).


{::comment}
### Colin Stedmon

![Colin Stedmon](img/colin.jpg){: style="float: left"}

Colin's current research is centred around chemical oceanography and environmental spectroscopy. Chemical oceanography is the study of the chemical composition of seawater and understanding how both physical and biological processes influence the biogeochemical cycling of elements in the sea. Spectroscopy is the study of the interaction of light with matter. In his research he use it as an approach to study the distribution and turnover (biogeochemistry) of matter in the ocean. This he does in both in the laboratory but also in the field as optical oceanography, which is in essence environmental spectroscopy. 

Colin's research builds on and now revives an earlier world leading position that Denmark had within the field optical oceanography (Jerlov, Copenhagen University) and takes it further by integrating it with modern chemical oceanography and new instrumental and computational capabilities. The organic matter spectroscopy techniques he has developed for oceanography has now spread to a wide range of other fields such as drinking and waste water treatment, recirculated aquaculture and ballast water assessment. 

Colin is a [Professor at the Technical University of Denmark](https://orbit.dtu.dk/en/persons/colin-stedmon).
{:/comment}