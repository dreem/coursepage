---
title: "The course"
bg: '#c7c9ea'
color: '#4d4d4d'
fa-icon: university
style: center
---


<span class="fa-stack subtlecircle" style="font-size:100px; background:rgba(255,166,0,0.1)">
  <i class="fa fa-circle fa-stack-2x text-white"></i>
  <i class="fa fa-eye fa-stack-1x text-black"></i>
</span>

## Overview

This online course will provide instruction on best-practice application of PARAFAC to fluorescence datasets, demonstrated using the drEEM toolbox for MATLAB.

 The course will be mainly delivered online using written documents, recorded lectures and videos. In person Question & Answer sessions will be held via Zoom on Mondays during each week of the course. To accommodate participants in different time zones, Zoom meetings will take place on Monday morning 0800-1000 CEST* then will be repeated at 1600-1800 CEST*. Recordings of these meetings will be available to participants who are unable to attend on either occasion.

 Participants are expected to upload an assignment during each week of the course. You can bring your own data or use the datasets provided. The expected workload is 10-30 hours per week depending on prior experience. All participants need to have access to MATLAB (2018 or later) and the ability to install the drEEM toolbox for MATLAB (admin rights not required).

 The course is part of a long-term collaboration between researchers at Chalmers University of Technology and the Technical University of Denmark.

If you are a student, you can obtain a certificate recommending the award of 5.0 ECTS credits after completing the [course](https://www.chalmers.se/sv/forskning/forskarutbildning/doktorandkurser/Sidor/institutionernas-doktorandkurser.aspx?course=FBOM116).  Its an important distinction as we can only award points to students enrolled in our PhD program.


 *Timezone: Central European Summer Time (CEST) +0200 UTC






<span class="fa-stack subtlecircle" style="font-size:100px; background:rgba(255,166,0,0.1)">
  <i class="fa fa-circle fa-stack-2x text-white"></i>
  <i class="fa fa-graduation-cap fa-stack-1x text-black"></i>
</span>

## Learning outcome
After completing the course, the student should be able to independently produce a validated PARAFAC model from fluorescence and absorbance measurements using the drEEM toolbox in MATLAB.

Students demonstrate their progress in the course by uploading assignments. A certificate recommending the award of 5 ECTS (Higher Education Credits) will be sent to participants that complete all three assignments.







<span class="fa-stack subtlecircle" style="font-size:100px; background:rgba(255,166,0,0.1)">
  <i class="fa fa-circle fa-stack-2x text-white"></i>
  <i class="fa fa-bullseye fa-stack-1x text-black"></i>
</span>

## Specific learning goals

1. Describe the theory of PARAFAC for fluorescence and its underlying assumptions
2. Describe why inner filter effects occur and how to deal with them
3. Clean a fluorescence dataset (identify and eliminate bad data)
4. Model a fluorescence EEM dataset
5. Validate the model using internal and external methods
6. Interpret a PARAFAC model of FDOM






<span class="fa-stack subtlecircle" style="font-size:100px; background:rgba(255,166,0,0.1)">
  <i class="fa fa-circle fa-stack-2x text-white"></i>
  <i class="fa fa-check-square fa-stack-1x text-black"></i>
</span>

## What will be covered? What will we discuss?
1. Content covered in videos/lectures
2. Content covered in papers where instructors were co-authors
3. Using the drEEM toolbox
4. Cleaning and modelling EEM datasets.
5. Validating and exporting PARAFAC models
6. Interpreting PARAFAC models of DOM fluorescence.






<span class="fa-stack subtlecircle" style="font-size:100px; background:rgba(255,166,0,0.1)">
  <i class="fa fa-circle fa-stack-2x text-white"></i>
  <i class="fa fa-times-circle fa-stack-1x text-black"></i>
</span>

## What will not be covered
The list of topics that users want to discuss is very long. Unforntunately we cannot cover everything. From previous experiences, we have to refrain from the following topics in the interest of time:

-     Using and calibrating your fluorometer
-     Measuring and applying spectral correction factors
-     EEM calibration, i.e. conversion to RU or QSE units
-     Implementing PARAFAC using different software platforms: e.g. PLS_Toolbox, staRdom, DOMFluor.




<span class="fa-stack subtlecircle" style="font-size:100px; background:rgba(255,166,0,0.1)">
  <i class="fa fa-circle fa-stack-2x text-white"></i>
  <i class="fa fa-tasks fa-stack-1x text-black"></i>
</span>


## The course schedule

The main part of the course will span over three weeks. Each week covers several topics that are introduced and discussed in a Zoom session. This session is supplemented with video material that participants can view on their own schedule. The topics are then also discussed in weekly assignments that are meant to deepen the understanding of each topic and show which software tools can be used for specific tasks. Here's a list of the topics per week:

![Course schedule](img/table.png){: style="float: center"}