---
title: "More information"
bg: '#ffc6b3'
color: '#4d4d4d'
style: center
fa-icon: question-circle
---

Are you interested and would like to receive announcements when information about the next course becomes available? Sign up here: [External website](https://stats.sender.net/forms/bWYRve/view)


You read the entire website but still want more information? Send us an email to [dreem@openfluor.net](mailto:dreem@openfluor.net?subject=Request%20for%20course%20information%20%22PARAFAC%20for%20fluorescence%22&body=Hi%20drEEM-team%2C%0D%0A%0D%0AI%20visited%20your%20course%20website%20%22Parallel%20Factor%20Analysis%20for%20DOM%20fluorescence%22%20and%20would%20like%20as%20much%20information%20as%20possible%20for%20my%20planning.%20Can%20you%20send%20a%20.pdf-file%20to%20me%20with%20all%20the%20information%3F%0D%0A%0D%0ASome%20additional%20questions%3A%0D%0A1.%0D%0A...%0D%0A%0D%0ABest%20regards%2C%0D%0A%0D%0AYour%20Name%20Here)